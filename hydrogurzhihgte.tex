% !TEX spellcheck = English US
% !TEX TS-program = pdflatex
% !TEX parameter = --shell-escape

\documentclass[aps,reprint,superscriptaddress,showpacs,showkeys,amsmath]{revtex4-1}
\usepackage{graphicx}
\usepackage[colorlinks=true,citecolor=blue,linkcolor=blue,urlcolor=black]{hyperref}
\usepackage[normalem]{ulem}
\usepackage{siunitx}


\DeclareGraphicsExtensions{.pdf,.ai,.png}
\DeclareGraphicsRule{.ai}{pdf}{.ai}{}


\begin{document}

\title{Observation of Gurzhi effect in the 3D topological insulator HgTe}

\author{Valentin L.\ M\"uller}
\email[Email: ]{valentin.mueller@physik.uni-wuerzburg.de}
\author{Yuan Yan}
%\email[Email: ]{yuan.yan@physik.uni-wuerzburg.de}
\affiliation{Experimentelle Physik III, Physikalisches Institut, Universit\"at W\"urzburg, 97074 W\"urzburg, Germany}
\author{Oleksiy Kashuba}
%\email[Email: ]{okashuba@physik.uni-wuerzburg.de}
\author{Bj\"orn Trauzettel}
%\email[Email: ]{trauzettel@physik.uni-wuerzburg.de}
\affiliation{Theoretische Physik IV, Institut f\"ur Theoretische Physik und Astrophysik, Universit\"at W\"urzburg, 97074 W\"urzburg, Germany}
\author{Johannes Kleinlein}
%\email[Email: ]{hartmut.buhmann@physik.uni-wuerzburg.de}
\author{Hartmut Buhmann}
%\email[Email: ]{hartmut.buhmann@physik.uni-wuerzburg.de}
\author{Laurens W.\ Molenkamp}
%\email[Email: ]{laurens.molenkamp@physik.uni-wuerzburg.de}
\affiliation{Experimentelle Physik III, Physikalisches Institut, Universit\"at W\"urzburg, 97074 W\"urzburg, Germany}


\begin{abstract}
Here we show temperature-dependent transport results obtained on strained HgTe layers by means of current heating.
\begin{center}
\vspace{-10pt}
{\it version~\input{|"git describe --tags"}}
%{\it version~0.3.0}
\vspace{-7pt}
\end{center}
\end{abstract}

%\pacs{1315, 9440T}
%\keywords{magnetic moment, solar neutrinos, astrophysics}
%\submitto{\jpg}

\maketitle
%\end{verbatim}


\section{Introduction}


The possibility to observe the hydrodynamical behavior in the solid state physics due to new breakthrough in the synthesis of new high mobility materials~\cite{???} has attracted a number of scientists performing the transport experiments on order to justify the strong interactions in the studied materials~\cite{Geim2016,Kim2016,Mackenzie2016}.
These experiment were supported by a number of theoretical works~\cite{Lucas2016,Lucas2018,Narozhny2017}.


We describe Gurzhi effect~\cite{Gurzhi1963,*Gurzhi1964,*Gurzhi1968} using the kinetic equation approach adapted for the Dirac systems~\cite{Kechedzhi2008,Muller2008b,Fritz2008,Kashuba2018}, which was previously successfully used for the modeling of Gurzhi effect in 2DEG~\cite{Molenkamp1994a,*Jong1995}.
In this work we pursue a study of the relativistic Gurzhi effect~\cite{Kashuba2018}.

The Dirac materials has a peculiarity of the electron-hole scattering and universal e-e relaxation rate if gated close to the charge neutrality point~\cite{Hartnoll2007,Fritz2008}.

\fbox{\emph{VALENTIN: Exceptional properties of HgTe }}

\section{Materials and Methods}



The experimental results are obtained on \SI{70}{\nm}-thick HgTe layers, which are fully strained due to epitaxial growth on a relaxed CdTe substrate~\cite{Brune2011,Leubner2016}.
The samples are structured into a Hall bar geometry by means of a low-energy electron beam lithography process in combination with ICP etching as described elsewhere [M\"uller?].
The resulting devices are micrometer-sized and have aspect ratios between 1:5 and 1:10.
The structures are covered by a gate stack consisting of $11$ alternating \SI{10}{\nm}-thick $\rm SiO_2$ and $\rm Si_3N_4$ layers as dielectric, a \SI{5}{\nm} thick $\rm Ti$ sticking layer, and a \SI{100}{\nm} thick $\rm Au$ electrode.
With a typical gate range of a few \SI{e11}{\cm^{-2}} we can drive the HgTe layer to be n- or p-conducting in the same device.
The mesa of a typical sample is shown in Fig.~\ref{fig:RVGRB}a), for clarity without the gate.

Most transport measurements were performed in a $\rm He_4$~cryostat at temperatures of \SI{1.4}{\K} and above.
Differential resistance vs.\ DC current at zero magnetic field was measured using standard low-frequency lock-in techniques, using a small AC voltage superimposed on the DC excitation.
All other characterization measurements were performed by pure AC measurements. 

\begin{figure}
\centering
\includegraphics[width=0.35\textwidth]{fig_setup}
\caption{%
Device layout and current heating.
a) Optical image of the device, for clarity shown before structuring a top gate on top.
The sketch also shows how we directly measure $dV/dI$ using two lock-in amplifiers.}
\label{fig:setup}
\end{figure}



\begin{figure}
\centering
\includegraphics[width=.49\textwidth]{fig_RVGRB}
\caption{%
Basic information about the experiment.
a) SEM of device with measurement circuit etc.\ is shown.
a) Shows the longitudinal resistance~$R_{xx}$ of the channel versus top gate voltage as black line.
Gate voltages are rescaled according to the zero density point, which is defined as $V_{0}$.
The colored dots indicate gate voltages, for which we present $dV/dI$ measurements in figure~\ref{fig:RI}\,a).
The inset (missing) shows a SEM micrograph of the measured device, together with a sketch how we performed measurements.
(MISSING) b), c), and d) show longitudinal resistance~$R_{xx}$ and transverse Hall resistance~$R_{xy}$ versus perpendicular magnetic field $B$ for different top gate voltages.
The indicated total carrier densities~$n_{\rm {tot}}$ are obtained by linear fits to~$R_{xy}$ in the range \SIrange{0.5}{1.5}{\tesla} for b) and d) and interpolated for c).}
\label{fig:RVGRB}
\end{figure}



\fbox{\emph{YUAN: Noise setup, temperature measurement:}}
\fbox{\emph{kT, shot noise, and other noise sources discussion}}

We describe Gurzhi effect~\cite{Gurzhi1963,*Gurzhi1964,*Gurzhi1968} using the kinetic equation approach adapted for the Dirac systems~\cite{Kechedzhi2008,Muller2008b,Fritz2008,Kashuba2018}, which was previously successfully used for the modeling of Gurzhi effect in 2DEG~\cite{Molenkamp1994a,*Jong1995}.
Our model includes the three sources of the electron scattering: walls, disorder, and the electron-electron scattering~\cite{Kashuba2018}.
We exploit stationary Boltzmann equation with two collision integrals describing short-potential disorder~\cite{Kechedzhi2008} and e-e scattering is described within the frame of Callaway ansatz~\cite{Callaway1959}.
The walls are described within the Fuchs-Sondheimer model in the limit of the scattering with zero specular probability~\cite{Fuchs1938,*Sondheimer1952}.



\section{Results and Discussion}

\paragraph{Basic device characterization}

Figure~\ref{fig:RVGRB} summarizes important characteristics of sample~1, measured on a $\SI{4x20}{\um}$ wide channel structure.
Figure~\ref{fig:RVGRB}c) shows the longitudinal resistance of the sample as function of top gate voltage, measured in a 4-point geometry as explained above.
The gate voltage is rescaled to $V_{0}$, which is the value at which the total carrier density is close to zero.
The carrier density was extracted from Hall measurements as shown in figures~\ref{fig:RVGRB}d), e), and f) for a selection of gate voltages.
The total density was extracted by linearly fitting the transverse resistance $R_{xy}$ as a function of magnetic field $B$ in the field range \SIrange{0.5}{1.5}{\tesla} (CITATION MISSING).
This is important, since the sample develops a pronounced nonlinearity for lower $B$-fields when going from high positive gate voltages towards the n to p transition and further to even lower gate voltages.
This nonlinearity can qualitatively be explained with 2-carrier behavior (CITATION MISSING), which is well-known to occur in systems which are  occupied by carriers with opposite charge and differing mobilities, which simultaneously contribute to conduction (CITATION MISSING).
For lower $B$-fields, the high-mobility species dominates the longitudinal and transverse resistance, while for higher fields both contribute according to (EQUATION MISSING).
We are thus able to extract the total carrier densities~$n_{\rm tot}$, which depend linearly on gate voltage~$V_{\rm g}$ as expected.
Figures~\ref{fig:RVGRB}d) and e) contain exemplary results of this analysis, while the value in figure~\ref{fig:RVGRB}f) is estimated by interpolating the high-n and high-p regimes. 

%!!!!!!!!!!  Needed: More quantitative estimates regarding two-carrier behaviour. Especially we need a way to roughly estimate the transition point from pure n-conductance to two-carrier behaviour, possibly also a temperature dependence of B to check how different densities are affected by temperature.  !!!!!!

The existence of two types of charge carriers can then also explain the peculiar longitudinal magnetoresistance for low magnetic fields. (EQUATIONS MISSING)
In the n-regime, where conduction is dominated by one type of carrier only, we observe a negative magnetoresistance below 200\,mT.
We attribute this behavior to the classical magneto-size effect, which occurs in narrow wires with diffusive boundary scattering, when the inelastic mean free path is in the order of the wire width~\cite{Thornton1989}. 

%!!!!!!!!!!  Needed: More quantitative estimates regarding magneto-size effect?  !!!!!!



\fbox{\emph{YUAN: Current heating calibration}}



\section{Results}





\begin{figure}
\centering
\includegraphics[width=0.49\textwidth]{fig_RI}
\caption{%
Experimental curves of temperature dependence in HgTe 3DTI.
a) and b) show differential resistance ($dV/dI$) vs.\ DC heating current $I_{\rm DC}$ for a \SI{4x20}{\um} wide channel sample at different gate voltages. The colors at different gate voltages correspond to those in figure~\ref{fig:RVGRB}\,a).
c) Temperature dependence of $dV/dI$ vs.\ $I_{\rm DC}$ for the \SI{4x20}{\um} device at a density of \SI{-1e11}{\cm^{-2}}.
The curves are stacked about \SI{0.4}{\kohm} for clarity.
The inset shows longitudinal resistance as function of bath temperature.  
d) Comparison of differential resistance vs.\ DC heating current in 2 devices, which are \SI{4x20}{\um} and \SI{2x10}{\um} in size, both at a density of \SI{-1e11}{\cm^{-2}}.
}
\label{fig:RI}
\end{figure}


The main results of this paper are shown in Fig.~\ref{fig:RI}a) and b), which show differential resistance $dV/dI$ as a function of DC heating current.
The differently colored curves correspond to different gate voltages, i.\,e. different chemical potential.
The color code is explained in Fig.~\ref{fig:RI}b), where $dV/dI$ at $I=0$ is superimposed on the gatesweep recorded with a pure AC measurement.
As one can see, there are distinct regimes in the temperature dependence of resistance.
For high n-type densities, there is hardly any temperature dependence. 
%!!!!!!!!!!  Give variation in percent?  !!!!!!
On the other hand, on the p-side the resistance monotonically increases with increasing temperature.
In between, there are two parts, one where the resistance is monotonically dropping with temperature ($V_{\rm g}-V_0 \gtrsim  \SI{0.3}{\V}$) and one where we observe a change from positive to negative temperature dependence ($V_{\rm g}-V_0 \lesssim \SI{0.3}{\V}$), with a distinct peak at some density-dependent DC current. 

In order to verify that the nonmonotonic behavior of $dV/dI$ with heating current is truly a hot electron effect, we have performed the same measurement at different bath (i.e.\ lattice) temperatures, thereby changing the electron temperature in a controlled manner.
Figure~\ref{fig:RI}c) shows differential resistance versus DC heating current for a fixed gate voltage, but different bath temperatures as indicated in the plot.
First of all, the pronounced peak starts to decrease for higher bath temperatures and eventually vanishes almost completely for temperatures exceeding approximately \SI{10}{\K}.
Second, tracing the resistance at zero current as function of bath temperature results in a curve that qualitatively resembles the DC heating current dependence of the differential resistance as shown in the inset of figure~\ref{fig:RI}c). 
It shows an increase with temperature, a pronounced peak at roughly \SI{10}{\K}, and a decrease for higher temperatures.
Taking these similarities, it is likely that the differential resistance is dominated by hot electron effects.
Furthermore, comparing peak positions gives a first estimate for the heating effect of a DC current. For the given gate voltage, a $I_{\rm DC}$ of \SI{3.3}{\uA} corresponds to an electron temperature of roughly \SI{10}{\K}.

We have also performed measurements on samples with different width.
The highest consistency among devices is given when they are structured on the same sample piece, so that the lithography is virtually identical for all devices.
Fig.~\ref{fig:RI}d) shows the differential resistance of the  \SI{4x20}{\um} device [cf.\ Fig.~\ref{fig:RI}a)] together with the differential resistance of a  \SI{2x10}{\um} device, which was simultaneously structured on the same piece of wafer.
Qualitatively, the two devices show a similar behavior upon DC current heating.
There are two quantitative differences, though.
First, the total resistance at zero current is higher for the  \SI{2x10}{\um} device, which is surprising, since the aspect ratio is the same as for the bigger device. We interpret this difference in terms of the classical size effect, as briefly discussed above.
%!!!!!!!!!!  More details on how to estimate difference quantitatively?  !!!!!!
The second difference is the value of the DC heating current at which $dV/dI$ reaches a maximum. It occurs for $I_{\rm DC} = \SI{2.4}{\uA}$ in the narrower channel and for $I_{\rm DC} = \SI{3.3}{\uA}$ in the wider channel.
This difference is 
%Interpretation?!

In order to verify that the nonmonotonic behavior of $dV/dI$ with heating current is truly a hot electron effect, we have performed the same measurement at different bath (i.e.\ lattice) temperatures, thereby changing the electron temperature in a controlled manner.
Fig.~\ref{fig:RI}d) shows differential resistance versus DC heating current for a fixed gate voltage, but different bath temperatures as indicated in the plot.
First of all, the pronounced peak starts to decrease for higher bath temperatures and eventually vanishes almost completely for temperatures exceeding approximately \SI{12}{\K}.
Second, tracing the resistance at zero current as function of bath temperature results in a curve that qualitatively resembles the dc heating current dependence of the differential resistance.
It shows an increase with temperature, a pronounced peak at roughly \SI{13}{\K}, and a decrease for higher temperatures.
Taking these similarities, it is likely that the differential resistance is dominated by hot electron effects.
Furthermore, comparing peak positions gives a first estimate for the heating effect if a DC current, i.e.\ for the given gate voltage, a $I_{\rm DC}$ of \SI{5.6}{\uA} corresponds to an electron temperature of roughly \SI{13}{\K}.

\begin{figure}
\centering
\vspace{10pt}
\includegraphics[width=.23\textwidth]{fig_A_Tfit}~%
\includegraphics[width=.23\textwidth]{fig_B_Tfit}
\caption{%
The position of the Gurzhi peak (in temperature units) as a function of the gating (in chemical potential units).
Different plots correspond to different samples, different color points correspond to opposite directions of the current.
The thin line correspond to the theoretical prediction of the Gurzhi peak, the dashed line corresponds to the theoretical prediction Bloch-Gr\"uneisen effect.}
\label{fig:Tfit}
\end{figure}

\begin{figure}
\centering
\vspace{10pt}
\includegraphics[width=.46\textwidth]{fig_A_fit}\\
\includegraphics[width=.46\textwidth]{fig_B_fit}
\caption{%
The comparison of the experimental $dV/dI$ curves (solid, left) with the theoretical predictions (dashed, right) at different gating (color resolved).
Two rows of figures correspond to the different samples.
}
\label{fig:fit}
\end{figure}


\section{Discussion}


\fbox{\emph{YUAN: Extra interpretation}}


The position of the Gurzhi peak, which appears in the clean sample can be estimated as $l_{ee}\sim W$, where $l_{ee}$ depends on the electron temperature.
The typical values of the electron densities in the experiment hint that the system is rather in Fermi liquid regime, than in charge neutrality one.
In this case the position of the peak can be estimated as $T_\text{Gurzhi}\sim \alpha^{-1}\sqrt{\mu W/\hbar v}$.
We illustrated this dependence of the peak position on the chemical potential in the Fig.~\ref{fig:Tfit}.
As we see, the expression for the peak temperature in Gurzhi effect fits the order of magnitude.
Additionally we showed the theoretical prediction according to the the Bloch-Gr\"uneisen effect, which may lead to the similar non-monotonic dependence of the resistance on electron temperature~\cite{Raichev2017}.
The peak temperature can be estimated as $T_\text{Bloch-Gr\"uneisen} = 2c_{s}p_{F}$, where $c_{s}$ is a sound velocity.
As we can see in Fig.~\ref{fig:Tfit}, this formula predicts temperatures much smaller than obtained in the experiment.





\section{Conclusions}

The paper is awesome.
\begin{acknowledgments}
Many thanks to all good people giving us money.
\end{acknowledgments}


\bibliographystyle{apsrev4-1}
\bibliography{hydrogurzhihgte}


\onecolumngrid
%\newpage
\appendix



%\renewcommand\theequation{SM\arabic{equation}}
%\setcounter{equation}{0}
%\renewcommand\thefigure{SM\arabic{figure}}
%\setcounter{figure}{0}

\begin{center}
{{\bf\large   Supplemental material for the Letter ``Observation of Gurzhi effect in the 3D topological insulator HgTe''}}
\end{center}

\section{Physics of Gurzhi effect}

The resistivity of conductors is determined by the electron scattering, which can be classified by the nature of its source.
For the samples with dominating disorder the scattering on the impurities dominates, leading to the Drudde picture.
In the clean samples at low temperatures the ballistic transport is characterized by the scattering on the edges, resulting in the mean free path of order of the smallest size of the sample (width in case of the channel geometry).
The electron-electron scattering does not fit this simple physical picture.
The e-e scattering, randomizing the momentum of a single electron, but preserving the net momentum of the scattering electrons, can as enhance, so suppress the electron conductivity.
If the number of e-e scattering events in the ballistic sample is much smaller than the scattering at the edge, as shown in the Fig.~\ref{fig:gurzhi}a), the extra e-e scattering helps tho randomize the momentum, passing a random momentum to another electron, which will dissipate it against the wall.
Thus, it leads to the increase of the resistivity.
The effect reverses if the mean free path dictated by the e-e scattering $l_{ee}$ is much shorter than the width of the channel $W$.
Then, the electrons in the the center of the channel move as a whole, just redistributing their momenta, without the net momentum dissipation.
The dissipation of the momentum described above, happens only in the areas of width $l_{ee}$ at the sides of the channel, see Fig~\ref{fig:gurzhi}b).
The increase of the e-e scattering length shortens $l_{ee}$, at thus decreases the net dissipation, leading to the decrease of the resistivity.
This non-monotonic dependence of the resistivity on the e-e scattering strength constitutes Gurzhi effect~\cite{Gurzhi1963,*Gurzhi1964,*Gurzhi1968}.


\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{fig_gurzhi}
\caption{%
The sketch illustrating the theory and the physics of Gurzhi effect.}
\label{fig:gurzhi}
\end{figure}

The e-e scattering in the systems with Dirac spectrum reveal an additional physics, when the system is driven in the charge neutrality point.
Strongly gated system reproduces the Fermi liquid behavior.
The e-e scattering is characterizes by the scattering rate $\tau_{ee}^{-1}\sim \alpha^{2}T^{2}/\mu$, where $\alpha$ is an effective electromagnetic field coupling constant, $T$ is temperature, and $\mu\gg T$ is the chemical potential counted from the Dirac point~\cite{Muller2008b}.
The effective e-e scattering length $l_{ee}=v\tau_{ee}$, where $v$ is Dirac velocity.
The e-e scattering redistributes the momenta of electrons, but since the velocities are bound to the momentum directions, the e-e scattering on it's own cannot dissipate the net drift velocity, and therefore the current.
In charge neutrality configuration ($\mu\ll T$) the scattering rate changes its dependence and equal to $\tau_{ee}^{-1}\sim \alpha^{2}T$~\cite{Muller2008b}.
In addition to the conventional e-e scattering, the scattering of the electron-like excitations (top cone, the velocity and momentum are collinear) and hole-like excitations (bottom cone, the velocity and momentum are anticollinear) is present.
Scattering an electron from one cone to another, even if the momentum is preserved, changes the direction of its velocity to the opposite.
Thus, e-h scattering does not preserve the net drift velocity, and therefore the current, resulting in finite resistivity of the Dirac system with the mean free path equal to e-e scattering length $l_{ee}=v\tau_{ee}$.


The charge neutrality state in experimentally relevant conditions has an additional peculiarity know as electron-hole puddles formation.
Random electrostatic potential created by charged imperfections in the bulk surrounding of the 2D Dirac state created the areas on n- and p- type of the surface state.
The interface between these areas is in principle penetrable for the electrons due to the Klein tunneling, but in case of the smooth change of the potential, only the particle incident close to the right angle to the interface can penetrate it.
This creates a finite resistance of the interface~\cite{Cheianov2006}, and therefore the conductivity of the sample if defined rather by a effective one-carrier-type path in the sample that connects the source and the drain, what lead to the effective narrowing of the channel width $W$. 


We exploit stationary Boltzmann equation
\begin{equation}
\mathbf{v} \cdot \partial_\mathbf{r} f + e\mathbf{E}\cdot\partial_\mathbf{p} f = \mathcal{I}_{\rm i}[f]+\mathcal{I}_{\rm ee}[f],
\label{eq:boltzmann}
\end{equation}
where $\mathcal{I}_{\rm i}$ is a collision integrals describing short-potential disorder~\cite{Kechedzhi2008} and e-e scattering is described by $\mathcal{I}_{\rm ee}$ within the frame of Callaway ansatz~\cite{Callaway1959}.
The walls are described within the Fuchs-Sondheimer model in the limit of the scattering with zero specular probability~\cite{Fuchs1938,*Sondheimer1952}. \begin{equation}
f_{\pm}(\mathbf{r}_\text{b},\mathbf{n}_\text{sc}p) = \bigl<f_{\pm}(\mathbf{r}_\text{b},\mathbf{n}_\text{in}p)\bigr>_{\mathbf{n}_\text{in}},
\label{eq:fuchsomm}
\end{equation}
where $\mathbf{r}_\text{b}$ is a point on the boundary, $\mathbf{n}_\text{in}$ and $\mathbf{n}_\text{sc}$, are directions of incident and scattered momenta, both of which have modulus $p$, and the angular brackets imply the averaging over all allowed directions $\mathbf{n}_\text{in}$ of the incident momenta.
Solving Eq.~\eqref{eq:boltzmann} with boundary conditions~\eqref{eq:fuchsomm}, we imply the driving electric field directed along the sample ($x$-axis), and the distribution function depends on momentum and $y$-coordinate (across the sample), as illustrated in Fig.~\ref{fig:gurzhi}a).


\section{Temperature measurements}

By applying a DC bias current ($I_\text{dc}$) through the channel sample with resistance $R$, a Joule heating power $P=I_\text{dc}^{2}R$ is injected into the electronic system.
At low enough bath temperature, as the electron-phonon interaction is weak, a temperature difference between the electron system and the phonon (or bath) will be introduced.
To investigate how the electron system get heated up, high-sensitivity Johnson noise thermometry (JNT) was used to independently measure the electron temperature~\cite{reference} .
The thermal noise power spectral density (PSD) due to the Brownian motion of the hot electrons was firstly filtered by a LC-tank circuit with center frequency $f_{0}=1/2\pi\sqrt{LC}=\SI{3}{\MHz}$ (to avoid the 1/f noise) and 3dB-bandwidth $\Delta f_{\SI{3}{\dB}}=1/2\pi RC=\SIrange[range-phrase = -]{0.1}{0.5}{\MHz}$ (depends on $R$).
Then the filtered signal was amplified by two independent extra low-noise amplifiers, the voltage pre-amplifier (homebuilt with HEMT ATF34143) sits on the \SI{1}{\K}-pot to reduce the intrinsic noise of the amplifier line and the room temperature amplifier (NF SA 220F5).
Finally the amplified signal was measured using a spectrum analyzer (HP8563E) with 5-times video average.
According to Nyquist theorem, the time averaged mean-square voltage $\langle V^{2}\rangle\equiv 4k_{B}T_{e}\Delta f \mathrm{Re}Z = 4k_{B}T_{e}\Delta f R$ at the resonant frequency of the $LC$ tank circuit, where $k_{B}$ is the Boltzmann constant, $T_{e}$ is the electron temperature, $Z$ is the complex impedance of the pre-amplifier see, and $\Delta f$ is the bandwidth. 
To calibrate the JNT setup, the thermal noise data of standard thin metal film resistors with meandering structure were collected at different bath temperatures (Figure~\ref{fig:TInoise} (a)(b)(c)).
Based on the Nyquist theorem, the power spectrum density is $S_{\nu}=G^{2}\times 4k_{B}(T_{e}+T_{N})R$, where $G$ is the total gain of the amplifier line including the insertion loss of radio frequency components and $T_{N}$ is the total system noise temperature which is unaffected by the bath temperature.
Utilizing the least squares fitting, the total gain of the amplifier chain calculated from the slope is about 801, and the corresponding system noise temperature when $T_{e}$ tends to zero is about \SI{2.2}{\K}.
To reduce the system error, the JNT set up was also calibrated on real samples, as resistances of the samples vary differently with gate voltage and bath temperature.
After that, the noise data of the device under test were collected via passing different DC bias current through under different gate voltages, and converted to electron temperature using the correspondingly calibrated $G$ and $T_{N}$ (Figure~\ref{fig:TInoise} (d)).

\begin{figure}
\centering
\includegraphics[width=0.65\textwidth]{fig_TInoise}
\caption{%
(a) Schematic of Johnson noise thermometry.
On one end of the device under test (DUT), a LC tank circuit including a \SI{5}{\micro\henry}-inductor and \SI{600}{\pico\farad}-capacitor was built in front of the low noise amplifier to avoid the 1/f noise contribution and filter the input signal.
While the other end of the DUT was shorted to ground by a \SI{100}{\nano\farad}-capacitor at high frequency.
(b) The thermal noise spectrum of standard thin metal film resistors at different bath temperatures range from \SI{25}{\milli\K} to about \SI{15}{\K}.
Inset: Schematic of the thin metal film (Ti/Au \SIlist[list-units = single, list-pair-separator = /]{5;80}{nm}) resistors with the meandering structure to ensure the good thermal contact with the bath.
(c) The Johnson noise vs temperature.
The solid line is linear fit, with an offset of \SI{2.2}{\K} due to the intrinsic amplifier line noise.
The total gain calculated from the slope is about 801.
(d) The representative electron temperature and DC bias current relationship of the \SI{4}{\um}-wide channel sample at different gate voltages with the corresponding charge neutrality point VCNP $\sim$\SI{3.25}{\V}. The bath temperature is \SI{1.1}{\K}. 
}
\label{fig:TInoise}
\end{figure}

Additionally, the electron temperatures were also obtained using the quantum Shubnikov-de Haas oscillations (SdHOs).
As shown in Figure~\ref{fig:TIsdho} (a)(b), the SdHOs were measured in a broad temperature and DC bias current range, with a clear thermal smearing in both cases.
By comparing the amplitude of the SdHOs at very low magnetic field under different bath temperature and DC bias, the relationship of the DC heating current and electron temperature was mapped (Figure~\ref{fig:TIsdho} (c)).
For comparison, the $T_{e}$ current density curves collected using the JNT at the same carrier concentration were replotted in Figure~\ref{fig:TIsdho} (c), which is consistent with the SdHOs results.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{fig_TIsdho}
\caption{%
(a) Bath temperature dependent magneto-resistance of a channel sample with width \SI{1}{\um} and length \SI{5}{\um}.
The classical size effect, the first increasing and then decreasing of the magneto-resistance around zero magnetic field, demonstrate the diffusive wall scattering in the channel sample.
(b) DC bias current dependent magneto-resistance of the same channel sample at \SI{1.4}{\K}.
By comparing the amplitude of the SdHOs at very low magnetic field under different bath temperature and DC bias, the relationship of the DC heating current and electron temperature was mapped.
(c) The electron temperature vs.\ DC bias current relations obtained via the SdHOs and the JNT, which are consistent with each other. }
\label{fig:TIsdho}
\end{figure}

The out-of-plane magnetic field ($B_{z}$) dependent differential resistance measurements were also conducted for several bath temperatures (Figure~\ref{fig:RITB}).
Similar to the temperature dependent behavior, the non-monotonic behavior vanished when $B_{z}$ was larger than \SI{60}{\milli\tesla}.
It is a clear demonstration that the electron cyclotron diameter ($D$) works as another characteristic scattering length to affect the momentum-relaxing electron-wall scattering by $1/l_\text{e-wall}=1/l_\text{e-e}+1/D$, where $l_\text{e-e}$ is the electron-electron scattering length, which stays the same with zero DC bias at fixed bath temperature.

\begin{figure}
\centering
\includegraphics[width=.65\textwidth]{fig_RITB}
\caption{%
(a) Bath temperature dependent differential resistance of a channel sample with width \SI{4}{\um} and length \SI{20}{\um} under zero magnetic field.
The non-monotonic behavior stays almost the same below \SI{1}{\K}, while it disappears once $T_\text{bath}>\SI{9}{\K}$.
The curves are shifted for clarity.
(b) Out-of-plane magnetic field dependent differential resistance of the same channel sample at $T=\SI{25}{\milli\K}$.
When the magnetic field is larger than \SI{60}{\milli\tesla} the non-monotonic behavior vanishes, which demonstrate the cyclotron diameter as another characteristic scattering length affects the momentum-relaxing electron-wall scattering. }
\label{fig:RITB}
\end{figure}


\end{document}

